﻿# Readme

PrefabInjector Unity3D tool by Meepolisk Nguyễn (hoanguyenduc1206@gmail.com)

Copyright (c) 2019

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

# Instructor
This tool is an open source Unity3D tool for fast switching prefab of game objects in scene view.
After install the unitypackage into your project, select RTool --> PrefabInjector on Unity menu to open the Prefab Injector window

![1](ReadMeRes/1.png)

# Video demo

https://www.youtube.com/watch?v=TWQvoCr5qME

# Download

https://gitlab.com/Meepolisk/prefabinjector/blob/master/PrefabInjector.unitypackage