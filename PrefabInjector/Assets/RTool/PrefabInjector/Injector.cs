﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// This class will create instance to temporary store scene gameobject selection and its prefab
/// </summary>
public class Injector
{
    public GameObject SceneObject { get; private set; }
    public GameObject PrefabObject { get; private set; }
    public string DirectoryPath { get; private set; }

    internal Injector(GameObject sceneObject, GameObject prefabObject, string directoryPath)
    {
        SceneObject = sceneObject;
        PrefabObject = prefabObject;
        DirectoryPath = directoryPath;
    }

    public GameObject InjectPrefab(GameObject prefabToInject, bool retainTransform, bool allowUndo, bool retainName)
    {
        if (PrefabObject == prefabToInject)
            return null;

        GameObject newObject = PrefabUtility.InstantiatePrefab(prefabToInject, SceneObject.transform.parent) as GameObject;
        if (retainTransform)
        {
            newObject.transform.localPosition = SceneObject.transform.localPosition;
            newObject.transform.localRotation = SceneObject.transform.localRotation;
            newObject.transform.localScale = SceneObject.transform.localScale;
        }
        newObject.transform.SetSiblingIndex(SceneObject.transform.GetSiblingIndex() + 1);
        if (retainName)
            newObject.name = SceneObject.name;

        if (allowUndo)
        {
            Undo.DestroyObjectImmediate(SceneObject);
            Undo.RegisterCreatedObjectUndo(newObject, "Prefab Replaced");
        }
        else
        {
            Object.DestroyImmediate(SceneObject);
        }
        return newObject;
    }
}