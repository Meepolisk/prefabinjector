﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UObject = UnityEngine.Object;
using System.IO;
using System.Linq;

public class InjectorWindow : EditorWindow, IHasCustomMenu
{
    #region const, field, property 
    private const string version = " v0.1f";
    private const string extension = ".prefab";
    private const float minSizeX = 300;
    private const float minSizeY = 350;

    private Vector2 scrollViewPosition = Vector2.zero;
    private int currentSelectedIndex = 0;
    private List<string> prefabDirectories = new List<string>();
    private string preferedDirectory;
    private string loadedDirectory { get; set; }
    private List<UObject> loadedPrefabList = new List<UObject>();

    private bool settingMode = false;
    private int collumnCount { get; set; }
    private int rowCount { get; set; }
    private Rect prefabPickerRect { get; set; }
    private Rect scrollViewContentRect { get; set; }

    private List<Injector> helperList = new List<Injector>();

    private float prefabSelectorElementSize = 120f;
    private const float prefabSelectorElementSizeStep = 5f;
    private const float prefabSelectorElementSizeMin = 60f;
    private const float prefabSelectorElementSizeMax = 150f;
    private bool autoSnapUponSelect = true;
    private bool hotKeyInSceneView = false;
    private bool retainTransform = true;
    private bool allowUndo = true;
    private bool retainName = false;

    private bool IsValidToInject()
        => helperList.Count > 0 && Application.isPlaying == false && prefabDirectories.Count > 0;
    #endregion

    #region Selection refresh / EditorWindow behaviours
    private void OnSelectionChange()
        => UpdateSelection();
    private void Awake()
    {
        Debug.Log("Prefab Injector initializing...");
        AssetPreview.SetPreviewTextureCacheSize(int.MaxValue);
        if (prefabDirectories.Count == 0)
            RefreshAllPosiblePrefabPath();
    }
    private void OnEnable()
    {
        UpdateSelection();
    }
    private void OnDisable()
    {
        UpdateSelection();
    }
    private void OnDestroy()
    {
        settingMode = false;
        SceneView.duringSceneGui -= OnSceneGUI;
    }
    private void OnFocus()
    {
        // Remove delegate listener if it has previously been assigned.
        SceneView.duringSceneGui -= OnSceneGUI;
        // Add (or re-add) the delegate.
        SceneView.duringSceneGui += OnSceneGUI;
        //UpdateSelection();
    }
    private void UpdateSelection()
    {
        if (Selection.gameObjects.Length == 0)
        {
            helperList.Clear();
            Repaint();
            return;
        }

        List<GameObject> selectedObject = new List<GameObject>(Selection.gameObjects);
        helperList.RemoveAll(helper => !selectedObject.Contains(helper.SceneObject));
        selectedObject.RemoveAll(gObject => helperList.Any(helper => helper.SceneObject == gObject));

        foreach (var gameObject in selectedObject)
        {
            if (string.IsNullOrEmpty(gameObject.scene.name)) //selection is not in scene
                continue;

            GameObject prefabObject = PrefabUtility.GetCorrespondingObjectFromOriginalSource(gameObject);
            if (prefabObject == null) //selection is not a prefab ref
                continue;

            string validString = AssetDatabase.GetAssetPath(prefabObject).Replace("Assets/", string.Empty);
            string directoryPath = GetPrefabPath(validString);

            helperList.Add(new Injector(gameObject, prefabObject, directoryPath));
        }
        RefreshPreferedDirectory();
        Repaint();
    }
    #endregion

    #region Window GUI
    private const float btnRefreshSize = 50f;
    private const float btnPanelHeight = 23f;
    private const float btnSettingWidth = 40f;

    private void OnGUI()
    {
        GUILayout.BeginVertical();
        Rect headerRect = EditorGUILayout.GetControlRect();
        headerRect.y += 5;
        Rect dropRect = new Rect(headerRect.x, headerRect.y, headerRect.width - btnRefreshSize, headerRect.height);
        Rect refreshRect = new Rect(dropRect.xMax + 2, headerRect.y, btnRefreshSize - 2f, headerRect.height);

        Rect bodyRect = new Rect(headerRect.x, headerRect.yMax + 3f,
            position.width - 10f, position.height - btnPanelHeight - dropRect.height - 20f);

        Rect btnPanelRect = new Rect(headerRect.x + 5, position.height - btnPanelHeight - 7f, position.width - 20, btnPanelHeight + 5f);
        float autoRectWidth = btnSettingWidth;
        if (autoRectWidth > position.width * 0.3f)
            autoRectWidth = position.width * 0.3f;
        Rect btnApplyRect = new Rect(btnPanelRect.x, btnPanelRect.y, btnPanelRect.width - btnSettingWidth, btnPanelRect.height - 5f);
        Rect tgSettingRect = new Rect(btnApplyRect.xMax, btnApplyRect.y, btnSettingWidth, btnApplyRect.height);
        GUILayout.EndVertical();

        if (settingMode == false)
        {
            DrawPathSelector(dropRect);
            DrawPathRefresher(refreshRect);
            DrawPrefabSelector(bodyRect, prefabSelectorElementSize);
            DrawReplaceButton(btnApplyRect);
        }
        else
        {
            DrawSettingHeader(headerRect);
            DrawSettingBody(bodyRect);
            DrawVersion(btnApplyRect);
        }
        DrawSettingButton(tgSettingRect);

        DetectHotkeyWindow();
    }
    private void OnSceneGUI(SceneView sceneView)
    {
        if (hotKeyInSceneView)
            DetectHotkeyScene();
    }

    private void DetectHotkeyWindow()
    {
        Event e = Event.current;
        if (e.type == EventType.KeyDown)
        {
            switch (e.keyCode)
            {
                case KeyCode.UpArrow:
                    SelectionNavigate(0, 1);
                    break;
                case KeyCode.DownArrow:
                    SelectionNavigate(0, -1);
                    break;
                case KeyCode.LeftArrow:
                    SelectionNavigate(-1, 0);
                    break;
                case KeyCode.RightArrow:
                    SelectionNavigate(1, 0);
                    break;
            }
        }
        if (e.control)
        {
            if (e.keyCode == KeyCode.Return)
                    ForceInjectPrefab();
            else if (e.isScrollWheel)
            {
                prefabSelectorElementSize = Mathf.Clamp(prefabSelectorElementSize + e.delta.y * -prefabSelectorElementSizeStep, prefabSelectorElementSizeMin, prefabSelectorElementSizeMax);
                Repaint();
            }
        }
    }
    private void DetectHotkeyScene()
    {
        Event e = Event.current;
        if (e.control)
        {
            if (e.isScrollWheel)
            {
                e.Use();
                QuickRotateObject(e.delta.y);
                return;
            }
            if (e.type == EventType.KeyDown)
            {
                if (e.keyCode == KeyCode.Return)
                {
                    ForceInjectPrefab();
                    return;
                }
                switch (e.keyCode)
                {
                    case KeyCode.UpArrow:
                        SelectionNavigate(0, 1);
                        break;
                    case KeyCode.DownArrow:
                        SelectionNavigate(0, -1);
                        break;
                    case KeyCode.LeftArrow:
                        SelectionNavigate(-1, 0);
                        break;
                    case KeyCode.RightArrow:
                        SelectionNavigate(1, 0);
                        break;
                }
            }
        }
    }
    private bool CatchButton(KeyCode keyCode)
        => Event.current.type == EventType.KeyDown && Event.current.keyCode == keyCode;
    #endregion

    #region Drawing GUI section
    private void DrawSettingHeader(Rect rect)
    {
        GUIStyle style = new GUIStyle(EditorStyles.centeredGreyMiniLabel);
        style.fontSize = 11;
        EditorGUI.LabelField(rect, new GUIContent("Setting"), style);
    }
    private void DrawSettingBody(Rect rect)
    {
        GUIStyle headerStyle = new GUIStyle(EditorStyles.centeredGreyMiniLabel);
        headerStyle.fontSize = 14;

        EditorGUI.HelpBox(rect, "", MessageType.None);
        Rect smallRect = new Rect(rect.x + 10, rect.y + 20, rect.width - 25, rect.height - 50);
        GUILayout.BeginArea(smallRect);
        GUILayout.BeginVertical();

        EditorGUILayout.LabelField(new GUIContent("General Setting"), headerStyle, GUILayout.Height(25));
        prefabSelectorElementSize = EditorGUILayout.Slider(new GUIContent("Cell Size", "The size of grid's cell.\nHotkey: CTRL + wheel scroll"),
            prefabSelectorElementSize, prefabSelectorElementSizeMin, prefabSelectorElementSizeMax);
        autoSnapUponSelect = GUILayout.Toggle(autoSnapUponSelect, new GUIContent("Snap scroll view on selection",
            "When select scene object, this will snap the scroll view to the selected root prefab object"));
        hotKeyInSceneView = GUILayout.Toggle(hotKeyInSceneView, new GUIContent("Scene View hotkey", "Enable using hotkey in scene view"));
        retainTransform = GUILayout.Toggle(retainTransform, new GUIContent("Retain transform", "Will the new prefab injected retain transform unchanged"));
        allowUndo = GUILayout.Toggle(allowUndo, new GUIContent("Allow Undo", "Will the UnityEditor record prefab change for Undo / Redo later"));
        retainName = GUILayout.Toggle(retainName, new GUIContent("Retain name", "Will the new prefab injected retain the name"));

        EditorGUILayout.LabelField(new GUIContent("Hotkey Window / SceneView"), headerStyle, GUILayout.Height(25));
        DrawLayoutHotkeyInformation("Grid selection", null, "ArrowKey", "Ctrl + ArrowKey");
        DrawLayoutHotkeyInformation("Inject Prefab", null, "Ctrl + Enter", "Ctrl + Enter");
        DrawLayoutHotkeyInformation("Adjust grid element size", null, "Ctrl + ScrollWheel", null);
        DrawLayoutHotkeyInformation("Rotate object", "Rotate the selected scene object", null, "Ctrl + ScrollWheel");

        EditorGUILayout.LabelField(new GUIContent("Information"), headerStyle, GUILayout.Height(25));
        if (GUILayout.Button(new GUIContent("Check for update and Help", "Open web browser to my gitlab page")))
            FuncVisitGitLab();

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
    private void DrawLayoutHotkeyInformation(string name, string tooltip, string hotkeyWindow, string hotkeySceneView)
    {
        Rect rect = EditorGUILayout.GetControlRect();
        Rect leftRect = new Rect(rect.x, rect.y, rect.width * 0.4f, rect.height);
        GUI.Label(leftRect, new GUIContent(name, tooltip));
        if (string.IsNullOrEmpty(hotkeyWindow) == false)
        {
            Rect rightRectA = new Rect(leftRect.xMax, rect.y, rect.width * 0.3f, rect.height);
            GUI.Label(rightRectA, new GUIContent(hotkeyWindow), EditorStyles.centeredGreyMiniLabel);
        }
        if (string.IsNullOrEmpty(hotkeySceneView) == false)
        {
            Rect rightRectB = new Rect(leftRect.xMax + rect.width * 0.3f, rect.y, rect.width * 0.3f, rect.height);
            GUI.Label(rightRectB, new GUIContent(hotkeySceneView), EditorStyles.centeredGreyMiniLabel);
        }
    }
    private void DrawVersion(Rect rect)
    {
        GUIStyle style = new GUIStyle(EditorStyles.centeredGreyMiniLabel);
        style.fontSize = 12;
        style.alignment = TextAnchor.LowerLeft;
        GUI.Label(rect, "PrefabInjector " + version, style);
    }

    private void DrawPathSelector(Rect rect)
    {
        if (prefabDirectories.Count > 0)
        {
            int selectedF = prefabDirectories.IndexOf(preferedDirectory);
            int newF = EditorGUI.Popup(rect, selectedF, prefabDirectories.ToArray());
            if (newF != selectedF)
            {
                preferedDirectory = prefabDirectories[newF];
                TryRefreshPrefabList(preferedDirectory);
            }
        }
        else
        {
            GUI.enabled = false;
            EditorGUI.Popup(rect, 0, new GUIContent[] { new GUIContent(preferedDirectory) });
            GUI.enabled = true;
        }
    }
    private void DrawPathRefresher(Rect rect)
    {
        if (GUI.Button(rect, EditorGUIUtility.IconContent("vcs_refresh", "Refresh all prefab folder")))
            RefreshAllPosiblePrefabPath();
    }
    private void DrawPrefabSelector(Rect rect, float elementSize)
    {
        if (loadedPrefabList.Count == 0)
            return;
        
        EditorGUI.HelpBox(rect, "", MessageType.None);
        prefabPickerRect = new Rect(rect.x + 5, rect.y + 5, rect.width, rect.height - 10);
        collumnCount = (int)((prefabPickerRect.width - 15f) / elementSize);
        if (loadedPrefabList.Count < collumnCount)
            collumnCount = loadedPrefabList.Count;

        rowCount = Mathf.CeilToInt((float)loadedPrefabList.Count / collumnCount);
        scrollViewContentRect = new Rect(prefabPickerRect.x, prefabPickerRect.y,
            collumnCount * elementSize, //width
            (rowCount * elementSize * 0.9f) + ((collumnCount - 1) * 8f)); //height (plus space beetween rows) 
        scrollViewPosition = GUI.BeginScrollView(prefabPickerRect, scrollViewPosition, scrollViewContentRect);
        GUIContent[] loadedPrefabGUIContentList = new GUIContent[loadedPrefabList.Count];
        for (int i = 0; i < loadedPrefabList.Count; i++)
        {
            UObject prefab = loadedPrefabList[i];
            Texture texture = AssetPreview.GetAssetPreview(prefab);
            if (texture != null)
                loadedPrefabGUIContentList[i] = new GUIContent(AssetPreview.GetAssetPreview(loadedPrefabList[i]), prefab.name);
            else
                loadedPrefabGUIContentList[i] = new GUIContent(prefab.name, null, prefab.name);
        }

        var newSelection = GUI.SelectionGrid(scrollViewContentRect, currentSelectedIndex, loadedPrefabGUIContentList, collumnCount);
        if (newSelection != currentSelectedIndex)
        {
            currentSelectedIndex = newSelection;
        }
        GUI.EndScrollView();
    }
    private void DrawReplaceButton(Rect rect)
    {
        bool isValidToInject = IsValidToInject();
        GUI.enabled = isValidToInject;
        string tooltip = isValidToInject ? "Replace selected item to current selected scene GameObject" :
            "You must select at least 1 game object in scene to perform this action";
        if (GUI.Button(rect, new GUIContent("Replace", tooltip), EditorStyles.miniButtonLeft))
            ForceInjectPrefab();
        GUI.enabled = true;
    }
    private void DrawSettingButton(Rect rect)
    {
        settingMode = GUI.Toggle(rect, settingMode, EditorGUIUtility.IconContent("ClothInspector.SettingsTool", "Setting and Help (comming soon)"), EditorStyles.miniButtonRight);
    }
    #endregion

    #region Internal function
    private void RefreshPreferedDirectory()
    {
        if (helperList.Count == 0)
            return;

        string newPreferedDirectory = helperList
            .GroupBy(q => q.DirectoryPath)
            .OrderByDescending(gp => gp.Count())
            .First().First().DirectoryPath;

        if (newPreferedDirectory != preferedDirectory)
            preferedDirectory = newPreferedDirectory;

        TryRefreshPrefabList(newPreferedDirectory);
    }
    private void RefreshAllPosiblePrefabPath()
    {
        prefabDirectories.Clear();

        string[] allfilePaths = Directory.GetFiles(Application.dataPath, "*" + extension, SearchOption.AllDirectories);
        int stringIndexToRemove = Application.dataPath.Length + 1; //+1 because the slash
        foreach (var item in allfilePaths)
        {
            string validInput = item.Substring(stringIndexToRemove).Replace("\\", "/");
            string path = GetPrefabPath(validInput);
            if (!prefabDirectories.Contains(path))
                prefabDirectories.Add(path);
        }
    }
    private void ForceInjectPrefab()
    {
        GameObject prefabObject = loadedPrefabList[currentSelectedIndex] as GameObject;
        List<GameObject> newSelection = new List<GameObject>(Selection.gameObjects);
        helperList.ForEach(helper =>
        {
            var newGameObject = helper.InjectPrefab(prefabObject, retainTransform, allowUndo, retainName);
            if (newGameObject != null)
                newSelection.Add(newGameObject);
        });
        Selection.objects = newSelection.ToArray();
    }
    private void TryRefreshPrefabList(string directoryPath)
    {
        if (directoryPath == loadedDirectory)
        {
            TryRefreshIndex();
            return;
        }

        loadedDirectory = directoryPath;
        loadedPrefabList.Clear();
        currentSelectedIndex = 0;
        string windowDirectory = Application.dataPath + "/" + directoryPath;
        string[] allFile = Directory.GetFiles(windowDirectory);
        foreach (var item in allFile)
        {
            if (item.Contains(extension) && item.Contains(".meta") == false)
            {
                string unityPath = "Assets/" + item.Substring(Application.dataPath.Length + 1).Replace("\\", "/");

                UObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(unityPath);
                if (prefab != null)
                    loadedPrefabList.Add(prefab);
            }
        }
        TryRefreshIndex();
    }
    private void TryRefreshIndex()
    {
        if (helperList.Count == 0)
            return;

        GameObject preferedPrefabName = helperList
                                  .GroupBy(q => q.PrefabObject)
                                  .OrderByDescending(gp => gp.Count())
                                  .First().First().PrefabObject;
        currentSelectedIndex = loadedPrefabList.IndexOf(preferedPrefabName);
        if (currentSelectedIndex < 0)
            currentSelectedIndex = 0;

        TrySnapScrollViewPosition();
    }
    private void TrySnapScrollViewPosition()
    {
        if (collumnCount == 0 || autoSnapUponSelect == false)
            return;

        int selectedRow = currentSelectedIndex / collumnCount;
        float selectionRatio = (float)(selectedRow + 1) / rowCount;
        float scrollViewTotalLenght = scrollViewContentRect.height - prefabPickerRect.height;
        float newPos = 0f;
        if (scrollViewTotalLenght > 0)
        {
            newPos = scrollViewTotalLenght * selectionRatio;
        }
        scrollViewPosition = new Vector2(0f, newPos);
    }
    private void SelectionNavigate(int x, int y)
    {
        if (y != 0)
        {
            if (currentSelectedIndex > collumnCount && y > 0)
            {
                currentSelectedIndex -= collumnCount;
                TrySnapScrollViewPosition();
                Repaint();
            }
            if (currentSelectedIndex < (loadedPrefabList.Count - 1 - collumnCount) && y < 0)
            {
                currentSelectedIndex += collumnCount;
                TrySnapScrollViewPosition();
                Repaint();
            }
        }
        else if (x != 0)
        {
            if (currentSelectedIndex > 0 && x < 0)
            {
                currentSelectedIndex -= 1;
                TrySnapScrollViewPosition();
                Repaint();
            }
            else if (currentSelectedIndex < loadedPrefabList.Count - 1 && x > 0)
            {
                currentSelectedIndex += 1;
                TrySnapScrollViewPosition();
                Repaint();
            }
        }
    }
    private void QuickRotateObject(float delta)
    {
        foreach (var helper in helperList)
        {
            Undo.RecordObject(helper.SceneObject, "Quick Rotating");
            helper.SceneObject.transform.localEulerAngles = new Vector3(helper.SceneObject.transform.localEulerAngles.x,
                helper.SceneObject.transform.localEulerAngles.y + 90f * delta,
                helper.SceneObject.transform.localEulerAngles.z);
        }
    }
    #endregion

    #region Static calls
    public static void AnalyzePrefab(string inputPath, out string name, out string path)
    {
        string[] nodeSplit = inputPath.Split('/');
        name = null; path = null;
        if (nodeSplit.Length == 1)
        {
            name = nodeSplit[0];
            name = name.Replace(extension, string.Empty);
        }
        else if (nodeSplit.Length > 1)
        {
            name = nodeSplit[nodeSplit.Length - 1];
            path = inputPath.Replace('/' + name, string.Empty);
            name = name.Replace(extension, string.Empty);
        }
    }
    public static string GetPrefabPath(string inputPath)
    {
        string[] nodeSplit = inputPath.Split('/');
        if (nodeSplit.Length > 1)
        {
            string name = nodeSplit[nodeSplit.Length - 1];
            return inputPath.Replace('/' + name, string.Empty);
        }
        return null;
    }
    #endregion

    #region ContextMenu
    private const string gitURL = "https://gitlab.com/Meepolisk/prefabinjector";

    [MenuItem("RTool / PrefabInjector")]
    public static void ShowWindow()
    {
        InjectorWindow window = (InjectorWindow)GetWindow(typeof(InjectorWindow), false, "", focus: true);
        window.titleContent = EditorGUIUtility.TrTextContent("Prefab Injector",
            "Emeow me fur more detail at: hoa.nguyenduc1206@gmail.com",
            "d_PrefabVariant Icon");
        window.minSize = new Vector2(minSizeX, minSizeY);
        window.Show();
    }

    public void AddItemsToMenu(GenericMenu menu)
    {
        menu.AddItem(new GUIContent("Check for Update", "Visit my GitLab"),
            false, FuncVisitGitLab);
    }

    private void FuncVisitGitLab()
    {
        Help.BrowseURL(gitURL);
    }
    #endregion
}